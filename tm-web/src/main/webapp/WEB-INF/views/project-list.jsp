<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h2>PROJECT LIST</h2>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">NAME</th>
        <th width="100%" align="left">DESCRIPTION</th>
        <th width="200" align="left">STATUS</th>
        <th width="200" align="left">DATE START</th>
        <th width="200" align="left">DATE FINISH</th>
        <th width="100" nowrap="nowrap" align="center">EDIT</th>
        <th width="100" nowrap="nowrap" align="center">DELETE</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}" />
            </td>
            <td>
                <c:out value="${project.name}" />
            </td>
            <td>
                <c:out value="${project.description}" />
            </td>
            <td>
                <c:out value="${project.status.displayName}" />
            </td>
            <td>
                <fmt:formatDate type="date" value="${project.dateStart}" />
            </td>
            <td>
                <fmt:formatDate type="date" value="${project.dateFinish}" />
            </td>
            <td align="center">
                <a href="/project/edit/${project.id}">EDIT</a>
            </td>
            <td align="center">
                <a href="/project/delete/${project.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px;">
    <button>CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp"/>


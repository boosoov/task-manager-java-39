//package com.rencredit.jschool.boruak.taskmanager.repository;
//
//import com.rencredit.jschool.boruak.taskmanager.api.repository.ISystemObjectRepository;
//import com.rencredit.jschool.boruak.taskmanager.endpoint.SessionDTO;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.springframework.stereotype.Repository;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Repository
//public class SystemObjectRepository implements ISystemObjectRepository {
//
//    @NotNull
//    private final Map<String, Object> objects = new HashMap<>();
//
//    @Nullable
//    @Override
//    public SessionDTO putSession(@NotNull final SessionDTO session) {
//        return (SessionDTO) objects.put("Session", session);
//    }
//
//    @Nullable
//    @Override
//    public SessionDTO getSession() {
//        return (SessionDTO) objects.get("Session");
//    }
//
//    @Nullable
//    @Override
//    public SessionDTO removeSession() {
//        return (SessionDTO) objects.remove("Session");
//    }
//
//    @Override
//    public void clearAll() {
//        objects.clear();
//    }
//
//}

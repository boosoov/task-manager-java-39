package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyFirstNameException extends AbstractClientException {

    public EmptyFirstNameException() {
        super("Error! First name is empty...");
    }

}

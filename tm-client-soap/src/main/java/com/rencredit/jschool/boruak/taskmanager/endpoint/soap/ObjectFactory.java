
package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.rencredit.jschool.boruak.taskmanager.endpoint.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateTask_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "createTask");
    private final static QName _CreateTaskResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "createTaskResponse");
    private final static QName _DeleteAllTasks_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "deleteAllTasks");
    private final static QName _DeleteAllTasksResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "deleteAllTasksResponse");
    private final static QName _DeleteTask_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "deleteTask");
    private final static QName _DeleteTaskById_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "deleteTaskById");
    private final static QName _DeleteTaskByIdResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "deleteTaskByIdResponse");
    private final static QName _DeleteTaskResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "deleteTaskResponse");
    private final static QName _ExistsTaskById_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "existsTaskById");
    private final static QName _ExistsTaskByIdResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "existsTaskByIdResponse");
    private final static QName _FindTaskByIdDTO_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "findTaskByIdDTO");
    private final static QName _FindTaskByIdDTOResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "findTaskByIdDTOResponse");
    private final static QName _GetListTasks_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "getListTasks");
    private final static QName _GetListTasksResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "getListTasksResponse");
    private final static QName _EmptyIdException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyIdException");
    private final static QName _EmptyUserIdException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyUserIdException");
    private final static QName _EmptyLoginException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyLoginException");
    private final static QName _EmptyTaskException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyTaskException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.rencredit.jschool.boruak.taskmanager.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateTask }
     * 
     */
    public CreateTask createCreateTask() {
        return new CreateTask();
    }

    /**
     * Create an instance of {@link CreateTaskResponse }
     * 
     */
    public CreateTaskResponse createCreateTaskResponse() {
        return new CreateTaskResponse();
    }

    /**
     * Create an instance of {@link DeleteAllTasks }
     * 
     */
    public DeleteAllTasks createDeleteAllTasks() {
        return new DeleteAllTasks();
    }

    /**
     * Create an instance of {@link DeleteAllTasksResponse }
     * 
     */
    public DeleteAllTasksResponse createDeleteAllTasksResponse() {
        return new DeleteAllTasksResponse();
    }

    /**
     * Create an instance of {@link DeleteTask }
     * 
     */
    public DeleteTask createDeleteTask() {
        return new DeleteTask();
    }

    /**
     * Create an instance of {@link DeleteTaskById }
     * 
     */
    public DeleteTaskById createDeleteTaskById() {
        return new DeleteTaskById();
    }

    /**
     * Create an instance of {@link DeleteTaskByIdResponse }
     * 
     */
    public DeleteTaskByIdResponse createDeleteTaskByIdResponse() {
        return new DeleteTaskByIdResponse();
    }

    /**
     * Create an instance of {@link DeleteTaskResponse }
     * 
     */
    public DeleteTaskResponse createDeleteTaskResponse() {
        return new DeleteTaskResponse();
    }

    /**
     * Create an instance of {@link ExistsTaskById }
     * 
     */
    public ExistsTaskById createExistsTaskById() {
        return new ExistsTaskById();
    }

    /**
     * Create an instance of {@link ExistsTaskByIdResponse }
     * 
     */
    public ExistsTaskByIdResponse createExistsTaskByIdResponse() {
        return new ExistsTaskByIdResponse();
    }

    /**
     * Create an instance of {@link FindTaskByIdDTO }
     * 
     */
    public FindTaskByIdDTO createFindTaskByIdDTO() {
        return new FindTaskByIdDTO();
    }

    /**
     * Create an instance of {@link FindTaskByIdDTOResponse }
     * 
     */
    public FindTaskByIdDTOResponse createFindTaskByIdDTOResponse() {
        return new FindTaskByIdDTOResponse();
    }

    /**
     * Create an instance of {@link GetListTasks }
     * 
     */
    public GetListTasks createGetListTasks() {
        return new GetListTasks();
    }

    /**
     * Create an instance of {@link GetListTasksResponse }
     * 
     */
    public GetListTasksResponse createGetListTasksResponse() {
        return new GetListTasksResponse();
    }

    /**
     * Create an instance of {@link EmptyIdException }
     * 
     */
    public EmptyIdException createEmptyIdException() {
        return new EmptyIdException();
    }

    /**
     * Create an instance of {@link EmptyUserIdException }
     * 
     */
    public EmptyUserIdException createEmptyUserIdException() {
        return new EmptyUserIdException();
    }

    /**
     * Create an instance of {@link EmptyLoginException }
     * 
     */
    public EmptyLoginException createEmptyLoginException() {
        return new EmptyLoginException();
    }

    /**
     * Create an instance of {@link EmptyTaskException }
     * 
     */
    public EmptyTaskException createEmptyTaskException() {
        return new EmptyTaskException();
    }

    /**
     * Create an instance of {@link TaskDTO }
     * 
     */
    public TaskDTO createTaskDTO() {
        return new TaskDTO();
    }

    /**
     * Create an instance of {@link AbstractDTO }
     * 
     */
    public AbstractDTO createAbstractDTO() {
        return new AbstractDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTask }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "createTask")
    public JAXBElement<CreateTask> createCreateTask(CreateTask value) {
        return new JAXBElement<CreateTask>(_CreateTask_QNAME, CreateTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "createTaskResponse")
    public JAXBElement<CreateTaskResponse> createCreateTaskResponse(CreateTaskResponse value) {
        return new JAXBElement<CreateTaskResponse>(_CreateTaskResponse_QNAME, CreateTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllTasks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "deleteAllTasks")
    public JAXBElement<DeleteAllTasks> createDeleteAllTasks(DeleteAllTasks value) {
        return new JAXBElement<DeleteAllTasks>(_DeleteAllTasks_QNAME, DeleteAllTasks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllTasksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "deleteAllTasksResponse")
    public JAXBElement<DeleteAllTasksResponse> createDeleteAllTasksResponse(DeleteAllTasksResponse value) {
        return new JAXBElement<DeleteAllTasksResponse>(_DeleteAllTasksResponse_QNAME, DeleteAllTasksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteTask }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "deleteTask")
    public JAXBElement<DeleteTask> createDeleteTask(DeleteTask value) {
        return new JAXBElement<DeleteTask>(_DeleteTask_QNAME, DeleteTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteTaskById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "deleteTaskById")
    public JAXBElement<DeleteTaskById> createDeleteTaskById(DeleteTaskById value) {
        return new JAXBElement<DeleteTaskById>(_DeleteTaskById_QNAME, DeleteTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteTaskByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "deleteTaskByIdResponse")
    public JAXBElement<DeleteTaskByIdResponse> createDeleteTaskByIdResponse(DeleteTaskByIdResponse value) {
        return new JAXBElement<DeleteTaskByIdResponse>(_DeleteTaskByIdResponse_QNAME, DeleteTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "deleteTaskResponse")
    public JAXBElement<DeleteTaskResponse> createDeleteTaskResponse(DeleteTaskResponse value) {
        return new JAXBElement<DeleteTaskResponse>(_DeleteTaskResponse_QNAME, DeleteTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsTaskById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "existsTaskById")
    public JAXBElement<ExistsTaskById> createExistsTaskById(ExistsTaskById value) {
        return new JAXBElement<ExistsTaskById>(_ExistsTaskById_QNAME, ExistsTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsTaskByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "existsTaskByIdResponse")
    public JAXBElement<ExistsTaskByIdResponse> createExistsTaskByIdResponse(ExistsTaskByIdResponse value) {
        return new JAXBElement<ExistsTaskByIdResponse>(_ExistsTaskByIdResponse_QNAME, ExistsTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIdDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "findTaskByIdDTO")
    public JAXBElement<FindTaskByIdDTO> createFindTaskByIdDTO(FindTaskByIdDTO value) {
        return new JAXBElement<FindTaskByIdDTO>(_FindTaskByIdDTO_QNAME, FindTaskByIdDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIdDTOResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "findTaskByIdDTOResponse")
    public JAXBElement<FindTaskByIdDTOResponse> createFindTaskByIdDTOResponse(FindTaskByIdDTOResponse value) {
        return new JAXBElement<FindTaskByIdDTOResponse>(_FindTaskByIdDTOResponse_QNAME, FindTaskByIdDTOResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListTasks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getListTasks")
    public JAXBElement<GetListTasks> createGetListTasks(GetListTasks value) {
        return new JAXBElement<GetListTasks>(_GetListTasks_QNAME, GetListTasks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListTasksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getListTasksResponse")
    public JAXBElement<GetListTasksResponse> createGetListTasksResponse(GetListTasksResponse value) {
        return new JAXBElement<GetListTasksResponse>(_GetListTasksResponse_QNAME, GetListTasksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyIdException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyIdException")
    public JAXBElement<EmptyIdException> createEmptyIdException(EmptyIdException value) {
        return new JAXBElement<EmptyIdException>(_EmptyIdException_QNAME, EmptyIdException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyUserIdException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyUserIdException")
    public JAXBElement<EmptyUserIdException> createEmptyUserIdException(EmptyUserIdException value) {
        return new JAXBElement<EmptyUserIdException>(_EmptyUserIdException_QNAME, EmptyUserIdException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyLoginException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyLoginException")
    public JAXBElement<EmptyLoginException> createEmptyLoginException(EmptyLoginException value) {
        return new JAXBElement<EmptyLoginException>(_EmptyLoginException_QNAME, EmptyLoginException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyTaskException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyTaskException")
    public JAXBElement<EmptyTaskException> createEmptyTaskException(EmptyTaskException value) {
        return new JAXBElement<EmptyTaskException>(_EmptyTaskException_QNAME, EmptyTaskException.class, null, value);
    }

}

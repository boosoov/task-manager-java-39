package restendpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IProjectsRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class IProjectsRestEndpointTest {

    IProjectsRestEndpoint projectsRestEndpoint;

    @Before
    public void init()  {
        projectsRestEndpoint = IProjectsRestEndpoint.client("http://localhost:8080/");
    }

    @After
    public void clearAll() {

    }

    @Test
    public void test() {
        projectsRestEndpoint.deleteAll();
        List<ProjectDTO> emptyList = projectsRestEndpoint.getListDTO();
        Assert.assertTrue(emptyList.isEmpty());
        List<ProjectDTO> listForSave = new ArrayList<>();
        listForSave.add(new ProjectDTO());
        listForSave.add(new ProjectDTO());
        listForSave.add(new ProjectDTO());
        projectsRestEndpoint.saveAll(listForSave);
        Assert.assertEquals(3, projectsRestEndpoint.count());
        Assert.assertEquals(3, projectsRestEndpoint.getListDTO().size());
        projectsRestEndpoint.deleteAll(listForSave);
        Assert.assertEquals(0, projectsRestEndpoint.count());
    }

}
